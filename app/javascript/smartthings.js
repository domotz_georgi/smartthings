
/* globals $, Firebase */
'use strict';

var smartThingsToken  = $.cookie('smartthings_token'),
    access_url        = $.cookie('access_url');


// var SECRET_KEY_BASE = 'ia7KWRX84rK71TclYObaqejSYU0xV2fYM7Y2LZR6'
// HARDCODED TOKENS //
console.log(smartThingsToken);
console.log(access_url);
//smartThingsToken = '3a09ce9e-ade0-493f-9a9c-26102c5e0fd7';
//access_url = '/api/smartapps/installations/9b537aa1-bd05-4322-896c-93959c029617';
// HARDCODED TOKENS //


access_url = 'https://graph-eu01-euwest1.api.smartthings.com' + access_url+ '/switches/'; //on';

if (smartThingsToken) { // Simple check for token
  // Create a reference to the API using the provided token
  var dataRef = new Firebase('https://georgitest.firebaseio.com/');
  dataRef.auth(smartThingsToken);

  // in a production client we would want to
  // handle auth errors here.

} else {
  // No auth token, go get on
  window.location.replace('/auth/smartthings');
}
generateButtons();

function generateButtons(){

    var json_string;
    access_url = 'https://graph-eu01-euwest1.api.smartthings.com/api/smartapps/installations/9b537aa1-bd05-4322-896c-93959c029617/switches';
    $.ajax({
        url: access_url,
        dataType: 'jsonp',
        type: 'GET',
        success: function(data) {
            json_string = JSON.stringify(data);
            drawButtons(json_string);
        }
    });

}

function drawButtons(json_string){
    var json_data = $.parseJSON(json_string);
    for (var i in json_data) {
        var button_name = json_data[i].name.replace(/\s/g,"-");
        var device_id = json_data[i].id;
        drawButton(button_name, device_id, "on");
        drawButton(button_name, device_id, "off");
    }

}

function drawButton(name,device_id, command){
    var inputElement = document.createElement('input');
    inputElement.type = "button";
    inputElement.value = name + ' ' + command;
    inputElement.addEventListener('click', function(){
        updateSwitch(device_id, command);
    });
    document.body.appendChild(inputElement);
}

function updateSwitch(id, command){
    var URI = "http://localhost:3000/switch?command=" + command + "&device_id=" + id
    $.ajax({
        url: URI,
        type: 'POST',
    });
}

function updatedRow(event){
    var device_id = event.device_id,
        device_name = event.device_name,
        event_name = event.event_name,
        event_value = event.event_value,
        event_date = event.event_date,
        hubId = event.hubId,
        event_description = event.event_description;

    return "" +
        "<tr>" +
        "<td>" + device_id          + "</td>" +
        "<td>" + device_name        + "</td>" +
        "<td>" + event_name         + "</td>" +
        "<td>" + event_value        + "</td>" +
        "<td>" + event_date         + "</td>" +
        "<td>" + hubId              + "</td>" +
        "<td>" + event_description  + "</td>" +
        "</tr>";

}

function updateView(event) {
    var row = updatedRow(event),
        device_id = event.device_id,
        old_row = $('#eventsTable tr > td:contains('+ device_id +')');

    if (old_row.length){
        old_row.parent().replaceWith(row);
    } else {
        $('#eventsTable').append(row);
    }

}

function firstChild(object) {
  for(var key in object) {
    return object[key];
  }
}
function lastChild(object) {
    var temp;
    for(var key in object) {
        temp = object[key];
    }
    return temp
}

/**
  Start listening for changes on this account,
  update appropriate views as data changes.

*/
dataRef.on('value', function (snapshot) {
    var events = snapshot.val().events,
        event;
    event = lastChild(events);
    updateView(event);
});
