'use strict';
var request = require('request'),
    express = require('express'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    app = express();

// Constants //
var ST_CLIENT_ID = "4671f2db-0386-49be-85a6-0a6f0819e256"
var ST_CLIENT_SECRET = "51bb7981-ecac-4a2a-9077-060a6998cc28"
var API_ENDPOINTS_URI = 'https://graph.api.smartthings.com/api/smartapps/endpoints';

// Configure Express app
app.use(cookieParser('cookie_secret_shh')); // Change for production apps
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(express.static(__dirname + '/app'));
app.use('/bower_components',  express.static(__dirname + '/bower_components'));
// End


var oauth2 = require('simple-oauth2')({
    clientID: ST_CLIENT_ID,
    clientSecret: ST_CLIENT_SECRET,
    site: 'https://graph.api.smartthings.com'
});

// Authorization uri definition
var authorization_uri = oauth2.authCode.authorizeURL({
    redirect_uri: 'http://localhost:3000/callback',
    scope: 'app',
    state: '3(#0/!~'
});

// Initial page redirecting to Github
app.get('/auth/smartthings', function (req, res) {
    res.redirect(authorization_uri);
});

// Callback service parsing the authorization token and asking for the access token
app.get('/callback', function (req, res) {
    var code = req.query.code;
    oauth2.authCode.getToken({
        code: code,
        redirect_uri: 'http://localhost:3000/callback'
    }, function saveToken(error, result) {
        if (error) { console.log('Access Token Error', error.message); }

        // result.access_token is the token, get the endpoint
        var bearer = result.access_token
        var sendreq = { method: "GET", uri: API_ENDPOINTS_URI + "?access_token=" + result.access_token };
        request(sendreq, function (err, res1, body) {
            var endpoints = JSON.parse(body);
            // we just show the final access URL and Bearer code
            var access_url = endpoints[0].url
            res.cookie('access_url', access_url);
            res.cookie('smartthings_token', bearer);
            res.redirect('/');
        });
    });
});
app.post('/switch', function (req, res) {
    console.log(req.query.command);
    var access_url = 'https://graph-eu01-euwest1.api.smartthings.com/api/smartapps/installations/9b537aa1-bd05-4322-896c-93959c029617/switches/';
    var token = req.cookies.smartthings_token,
        command = req.query.command,
        device_id = req.query.device_id;
    var URI = access_url + device_id + '/' + command;
    console.log(URI);
    var options = {
        uri: URI,
        method: 'PUT',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + token
        }
    };
    request(options, function (err, response) {
        //console.log('err ' + err);
        //console.log('res1 ' + JSON.stringify(response));
    });
});


module.exports = app;
